package com.gembelelit_listkehidupan.hitungberat

import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.CalendarContract
import android.provider.ContactsContract
import android.text.TextUtils
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        tv_nilai.visibility = View.GONE
        tv_index.visibility = View.GONE
        iv_gambar.visibility = View.GONE
        bn_share.visibility=View.GONE

        bn_hitung.setOnClickListener {

            var namaString  = et_nama.text.toString()
            var praktikumString = et_praktikum.text.toString()
            var assesmentsatuString = et_assesment1.text.toString()
            var assesmnetduaString = et_assesment2.text.toString()

            if(TextUtils.isEmpty(namaString)){
                et_nama.setError("required nama")
            }
            else if (TextUtils.isEmpty(praktikumString)){
                et_praktikum.setError("required praktikum")
            }
            else if(TextUtils.isEmpty(assesmentsatuString)){
                et_assesment1.setError("required assesment1")
            }
            else if(TextUtils.isEmpty(assesmnetduaString)){
                et_assesment2.setError("required assesment 2")
            }
            else {
                tv_nilai.visibility = View.VISIBLE
                tv_index.visibility = View.VISIBLE
                iv_gambar.visibility = View.VISIBLE
                bn_share.visibility= View.VISIBLE

                var praktikumInteger = praktikumString.toDouble()
                var assesmentsatuInteger = assesmentsatuString.toDouble()
                var assemsentduaInteger = assesmnetduaString.toDouble()
                var totalNilai=0.0
                praktikumInteger=praktikumInteger*0.2
                assesmentsatuInteger=assesmentsatuInteger*0.4
                assemsentduaInteger=assemsentduaInteger*0.4

                if (rb_ya.isChecked()) {
                    totalNilai=praktikumInteger+assesmentsatuInteger+assemsentduaInteger
                    if(totalNilai>80){
                        tv_nilai.setText(totalNilai.toString())
                        tv_index.setText("A")
                        iv_gambar.setImageResource(R.drawable.ic_insert_blue)
                    }
                    if(totalNilai<=80 && totalNilai>70){
                        tv_nilai.setText(totalNilai.toString())
                        tv_index.setText("B")
                        iv_gambar.setImageResource(R.drawable.ic_insert_blue)
                    }
                    if(totalNilai<=70 && totalNilai>60){
                        tv_nilai.setText(totalNilai.toString())
                        tv_index.setText("C")
                        iv_gambar.setImageResource(R.drawable.ic_insert_blue)
                    }
                    if(totalNilai<=60 && totalNilai>50){
                        tv_nilai.setText(totalNilai.toString())
                        tv_index.setText("D")
                        iv_gambar.setImageResource(R.drawable.ic_insert_emoticon_green)
                    }
                    if(totalNilai<=50){
                        tv_nilai.setText(totalNilai.toString())
                        tv_index.setText("E")
                        iv_gambar.setImageResource(R.drawable.ic_insert_emoticon_green)
                    }
                } else if (rb_tidak.isChecked()) {
                    totalNilai=assesmentsatuInteger+assemsentduaInteger
                    if(totalNilai>80){
                        tv_nilai.setText(totalNilai.toString())
                        tv_index.setText("A")
                        iv_gambar.setImageResource(R.drawable.ic_insert_blue)
                    }
                    if(totalNilai<=80 && totalNilai>70){
                        tv_nilai.setText(totalNilai.toString())
                        tv_index.setText("B")
                        iv_gambar.setImageResource(R.drawable.ic_insert_blue)
                    }
                    if(totalNilai<=70 && totalNilai>60){
                        tv_nilai.setText(totalNilai.toString())
                        tv_index.setText("C")
                        iv_gambar.setImageResource(R.drawable.ic_insert_blue)
                    }
                    if(totalNilai<=60 && totalNilai>50){
                        tv_nilai.setText(totalNilai.toString())
                        tv_index.setText("D")
                        iv_gambar.setImageResource(R.drawable.ic_insert_emoticon_green)
                    }
                    if(totalNilai<=50){
                        tv_nilai.setText(totalNilai.toString())
                        tv_index.setText("E")
                        iv_gambar.setImageResource(R.drawable.ic_insert_emoticon_green)
                    }
                } else {
                    Toast.makeText(this, "tolong check salah satu", Toast.LENGTH_SHORT).show()
                }
                bn_share.setOnClickListener {
                    var data = arrayOf("mopro.d3ifcoll@gmail.com")
                    var subject : String = namaString
                    var message : String = "nama" + namaString + "\n"
                    message = message + "total nilai" + totalNilai.toString() + "\n"
                    message = message + "index nilai" + tv_index.text.toString() + "\n"
                    message = message + "praktikum" + praktikumInteger.toString() + "\n"
                    message = message + "assementsatu" + assesmentsatuInteger.toString() + "\n"
                    message = message + "assementdua" + assemsentduaInteger.toString() + ""
                    gmailKirim(data, subject , message)
                }
            }
        }
    }
    fun gmailKirim (address : Array<String>, subject: String ,message : String) {
       var intent = Intent(Intent.ACTION_SENDTO).apply {
           type="*/*"
           setData(Uri.parse("mailto:"))
           putExtra(Intent.EXTRA_EMAIL, address)
           putExtra(Intent.EXTRA_SUBJECT, subject)
           putExtra(Intent.EXTRA_STREAM, message)
       }
        if(intent.resolveActivity(packageManager)!=null) {
            startActivity(intent)
        }
        else{
            Toast.makeText(this,"aplikasi tidak terdapat yang mendukung", Toast.LENGTH_SHORT).show()
        }
    }

}
